<h1>Projeto de Mineração de Dados / BI</h1>

<h2> Escopo de Projeto </h2>

*  Análise de requisitos 
*  Tratamento do DATSET/Limpeza
*  Separar
    *   Bases de treinamento
    *   Base de teste
*  Selecionar algoritimo de ML
*  Validação do modelo
*  Testar modelo

<h3>BI</h3>

*  Coleta
*  ETL
*  Paineis

<h2>Data de entrega</h2>
O trabalho esta previsto para ser concluido no dia <b><i>28 de novembro de 2019</i></b>

<h2>Trello</h2>
[Quadro do projeto](https://trello.com/b/P8vapMu3)
